// Bianca Rossetti 2233420
import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		WaffleMaker[] waffleMakers = new WaffleMaker[4];
		int cookTime = 0;
		int tempF = 0;
		String patternType = " ";
		
		for (int count = 0; count < waffleMakers.length; count++)
		{
			System.out.println("Average cook time of the waffle maker in minutes?");
			cookTime = Integer.parseInt(scan.nextLine());
			
			System.out.println("Temperature of the waffle maker, in Fahrenheit?");
			tempF = Integer.parseInt(scan.nextLine());
			
			System.out.println("Pattern design of the waffle maker?");
			patternType = scan.nextLine();
			
			waffleMakers[count] = new WaffleMaker(cookTime, patternType, tempF);
		}
		
		System.out.println("Average cook time of the last waffle maker: " + waffleMakers[(waffleMakers.length - 1)].getAverageCookTimeMinutes() + " minutes");
		System.out.println("Temperature of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getTemperatureFahrenheit() + "F");
		System.out.println("Pattern design of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getPatternType());
		
		waffleMakers[0].finishedCooking();
		waffleMakers[0].patternTypes();
		/* ------------------------------------------------------ */
		System.out.println("PART 2");
		waffleMakers[1].changeTempSetting(200);
		// print to verify change is good
		System.out.println("New temp: " + waffleMakers[1].getTemperatureFahrenheit());
		
		// updating last appliance once again
		// print old values:
		System.out.println("Average cook time of the last waffle maker: " + waffleMakers[(waffleMakers.length - 1)].getAverageCookTimeMinutes() + " minutes");
		System.out.println("Temperature of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getTemperatureFahrenheit() + "F");
		System.out.println("Pattern design of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getPatternType());
		
		// get input from user to change values of fields:
		System.out.println("New cook time, in minutes");
		waffleMakers[(waffleMakers.length - 1)].setAverageCookTimeMinutes(Integer.parseInt(scan.nextLine()));
		System.out.println("New temperature, in Fahrenheit");
		waffleMakers[(waffleMakers.length - 1)].setTemperatureFahrenheit(Integer.parseInt(scan.nextLine()));
		
		// print new values:
		System.out.println("Average cook time of the last waffle maker: " + waffleMakers[(waffleMakers.length - 1)].getAverageCookTimeMinutes() + " minutes");
		System.out.println("Temperature of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getTemperatureFahrenheit() + "F");
		System.out.println("Pattern design of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getPatternType());
	}
}

/*
import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		WaffleMaker[] waffleMakers = new WaffleMaker[4];
		
		for (int count = 0; count < waffleMakers.length; count++)
		{
			waffleMakers[count] = new WaffleMaker();
			
			System.out.println("Average cook time of the waffle maker in minutes?");
			waffleMakers[count].setAverageCookTimeMinutes(Integer.parseInt(scan.nextLine()));
			
			System.out.println("Temperature of the waffle maker, in Fahrenheit?");
			waffleMakers[count].setTemperatureFahrenheit(Integer.parseInt(scan.nextLine()));
			
			System.out.println("Pattern design of the waffle maker?");
			waffleMakers[count].setPatternType(scan.nextLine());
		}
		
		System.out.println("Average cook time of the last waffle maker: " + waffleMakers[(waffleMakers.length - 1)].getAverageCookTimeMinutes() + " minutes");
		System.out.println("Temperature of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getTemperatureFahrenheit() + "F");
		System.out.println("Pattern design of the last waffle maker: " +  waffleMakers[(waffleMakers.length - 1)].getPatternType());
		
		waffleMakers[0].finishedCooking();
		waffleMakers[0].patternTypes();
		
		System.out.println("PART 2");
		waffleMakers[1].changeTempSetting(200);
		System.out.println(waffleMakers[1].getTemperatureFahrenheit());
	}
}
*/