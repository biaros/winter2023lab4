// Bianca Rossetti 2233420
public class WaffleMaker
{
	private int averageCookTimeMinutes;
	private String patternDesign;
	private int temperatureFahrenheit;
	
	public void finishedCooking()
	{
		System.out.println("its been " + averageCookTimeMinutes + " minutes, your waffle is finished!");
		
	}
	
	public void patternTypes()
	{
		System.out.println("Your waffle has " + patternDesign + " patterns!");
	}
	
	// part 2
	public void changeTempSetting(int newTemp) // is this right???
	{
		if(isValidTemp_Time(newTemp))
		{
			this.temperatureFahrenheit = newTemp;
		}
		else
		{
			System.out.println("Invalid temperature setting");
		}
	}
	
	private boolean isValidTemp_Time(int newTemp_Time)
	{
		return(newTemp_Time > 0);
	}
	
	// getter methods
	public int getAverageCookTimeMinutes()
	{
		return(this.averageCookTimeMinutes);
	}
	public String getPatternType()
	{
		return(this.patternDesign);
	}
	public int getTemperatureFahrenheit()
	{
		return(this.temperatureFahrenheit);
	}
	
	// setter methods
	public void setAverageCookTimeMinutes(int cookTimeMins)
	{
		if(isValidTemp_Time(cookTimeMins))
		{
			this.averageCookTimeMinutes = cookTimeMins;
		}
		else
		{
			System.out.println("Invalid input");
		}
	}
	
	public void setTemperatureFahrenheit(int newTempFahrenheit)
	{
		if(isValidTemp_Time(newTempFahrenheit))
		{
			this.temperatureFahrenheit = newTempFahrenheit;
		}
		else
		{
			System.out.println("Invalid input");
		}
	}
	
	// constructor
	public WaffleMaker(int cookTimeMins, String patternTypes, int averageTempFahrenheit)
	{
		this.averageCookTimeMinutes = cookTimeMins;
		this.patternDesign = patternTypes;
		this.temperatureFahrenheit = averageTempFahrenheit;
	}
}